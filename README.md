정우람 wooram Jeong

- Android Developer
- Mail : <wrjeong@nate.com>  
- github : https://github.com/wooramjeong
<br/><br/>

근무 회사 ( 총 개발 경력 : 7년 6개월 )
-
### 이상투자그룹
* 2018.07 ~ 2018.10 (3개월)

### BLT
* 2017.05 ~ 2018.05 (1년)

### 지티시스템
* 2015.05 ~ 2017.05 (2년)

### 텔레웍스
* 2013.01 ~ 2014.01 (1년1개월)

### 모바일젠
* 2009.01 ~ 2012.03 (3년2개월)
<br/><br/>

안드로이드 프로젝트 ( 안드로이드 개발 경력 : xx )
-
#### 이상투자그룹 주식 투자자문용 서비스 앱 ( 하이브리드 )
* 회사 : 이상투자그룹
* 앱소개 : 주식 주자 자문용 안드로이드 하이브리드 앱
* 개발기술 : Java, Retrofit2, Okhttp3, facebook-share, kakaotalk-link, firebase-fcmpush
* 담당업무 : 개발
* 개발기간 : 3개월

<img src="images/stockOnAir01.png" width="160"/> <img src="images/stockOnAir02.png" width="160"/> <img src="images/stockOnAir03.png" width="160"/> <img src="images/stockOnAir05.png" width="160"/> <img src="images/stockOnAir06.png" width="160"/> <img src="images/stockOnAir07.png" width="160"/> <img src="images/stockOnAir04.png" width="160"/>

#### BLT자체 개발 32인치 안드로이드 기반 테블릿 장치의 동영상 서비스 통합 앱 ( 네이티브 )
* 회사 : BLT
* 앱소개 : BLT 자체 개발  안드로이드 기반 32인치테블릿 장치의 동영상 서비스(유튜브, 아프리카TV, 네이버Tv, 트위치, 카카오TV, 판도라TV, V라이브 )통합앱 
* 개발기술 : Java, Jsoup, Glide, Retrofit2, Gson, Youtube api, Twitch api
* 담당업무 : 개발
* 개발기간 : 1년
* 상세설명 : https://www.tvlet.co.kr/dabom

<img src="images/dabom.png" width="748"/>

#### BLT자체 개발 테블릿 컨트롤을 위한 리모컨앱 ( 네이티브 )
* 회사 : BLT
* 앱소개 : BLT 자체 개발 32인치 테블릿 컨트롤을 위한 리모컨
* 개발기술 : Java, Glide, crashlytics
* 담당업무 : 개발
* 개발기간 : 1년
* 플레이스토어 : https://play.google.com/store/apps/details?id=kr.co.tvlet.blt.rcuclient

<img src="images/rcuclient01.png" width="160"/> <img src="images/rcuclient02.png" width="160"/> <img src="images/rcuclient03.png" width="160"/> <img src="images/rcuclient04.png" width="160"/> <img src="images/rcuclient05.png" width="160"/> <img src="images/rcuclient07.png" width="160"/> <img src="images/rcuclient08.png" width="160"/> <img src="images/rcuclient09.png" width="160"/>

![Alt text](https://bitbucket.org/wrjeoung/profile/src/master/images/rcuclient01.png )

#### 팬택(SKY) 프리로드로 탑재되는 SNS통합 앱 ( 네이티브 )
* 회사 : 모바일젠
* 앱소개 : facebook, linkdIn, twitter 등의 SNS를 통합 구현한 앱
* 개발기술 : Java
* 담당업무 : 개발 및 유지보수
* 개발기간 : 4개월
* 플레이스토어 : 팬택(SKY) 프리로드로 탑재되는 앱으로 플레이스토어에 발행되지 않음.







